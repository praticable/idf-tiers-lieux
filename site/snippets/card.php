<li class="card" x-data="{
  title: '<?= $target->title() ?>'.toLowerCase(),
  intro: '<?= $target->intro() ?>'.toLowerCase(),
  tags: '<?= $target->tags() ?>'.split(', '),
  get isVisible() {
    const searchLower = search.toLowerCase();
    const isTitleMatch = this.title.toLowerCase().includes(searchLower);
    const isIntroMatch = this.intro.toLowerCase().includes(searchLower);
    const isTagMatch = this.tags.some(tag => filters.includes(tag));

    return isNoSearch || (search.length > 0 && (isTitleMatch || isIntroMatch)) || isTagMatch;
}

}" :class="isVisible ? '': 'hidden'">
  <a href="<?= $target->url() ?>" class="card-link">
    <?php if ($target->cover()->isNotEmpty()): ?>
      <figure class="image-wrapper image-wrapper--half-radius">
        <div class="color-filter"></div>
        <img src="<?= $target->cover()->toFile()->url() ?>" alt="">
      </figure>
    <?php endif ?>
    <div class="card__content">
      <p class="card__date"><?= $target->published()->toDate('d/m/Y') ?></p>
      <h3 class="card__title"><?= $target->title() ?></h3>
      <div class="card__intro">
        <?= $target->intro()->short(180)->kt() ?>
    </div>
      <?php if ($target->tags()->isNotEmpty()): ?>
        <div class="card__tags tags">
          <?php foreach($target->tags()->split() as $tag): ?>
            <span class="card__tag tag"><?= $tag ?></span>
          <?php endforeach ?>
        </div>
      <?php endif ?>
    </div>
    <span class="white-line--card"></span>
  </a>
</li>