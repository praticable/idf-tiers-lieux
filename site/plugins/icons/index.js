panel.plugin("adrienpayet/icons", {
  icons: {
    "side-by-side-images":
      '<path d="M7,3V13H5v2H8a1,1,0,0,0,1-1V4h2V2H8A1,1,0,0,0,7,3Z" /><circle cx="2" cy="14" r="2" /><polygon points="12 0 12 6 16 3 12 0" />',
  },
});
