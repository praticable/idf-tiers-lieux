import GenerateStructure from "./components/GenerateStructure.vue";
import DataDiag from "./components/DataDiag.vue";

window.panel.plugin("getkirby/pluginkit", {
  fields: {
    "data-diag": DataDiag,
    // "generate-structure": GenerateStructure,
  },
});
