<?php
  $modifiers = $modifiers ?? false;
  $burger = $burger ?? false;
  $idAttribute = isset($id) ? 'id="' . $id . '"' : false;
  $variant = $variant ?? false;
  $hrefAttribute = isset($href) ? 'href="' . $href . '"' : false;
  $tag = isset($href) ? 'a' : 'button';
?>

<<?= $tag ?> class="
  button
  <?php e($variant, $variant) ?>
  <?php 
  if ($modifiers) {
    foreach ($modifiers as $modifier) {
      echo ' button--' . $modifier;
    } 
  }
  ?>
" type="button" <?php e($idAttribute, $idAttribute) ?> <?php e($hrefAttribute, $hrefAttribute) ?>>
  <span class="white-line white-line--button white-line--button-top"></span>
  <?= $text ?>
  <?php if ($variant === 'mobile'): ?>
    <span id="burger-icon"></span>
  <?php endif ?>
  <span class="white-line white-line--button white-line--button-bottom"></span>
</<?= $tag ?>>