<?php

use Kirby\Data\Yaml;

function csvStringToYaml($csvString) {
  $lines = explode("\n", $csvString);
  $csv = array_map(function($line) {
      $cells = str_getcsv($line, ',');
      $isEmpty = strlen(implode($cells)) === 0;
      if (!$isEmpty) {
        return [
          'name' => isset($cells[0]) ? $cells[0] : '',
          'activity' => isset($cells[1]) ? str_replace([' ; ', ' ;', ';', '; '], ', ', $cells[1]) : '',
          'street' => isset($cells[2]) ? $cells[2] : '',
          'town' => isset($cells[3]) ? $cells[3] : '',
          'department' => isset($cells[4]) ? $cells[4] : '',
          'mail' => isset($cells[5]) ? $cells[5] : '',
          'network' => isset($cells[6]) ? $cells[6] : '',
          'link' => isset($cells[7]) ? $cells[7] : '',
          'linklabel' => '',
          'cover' => '',
          'description' => ''
        ];
      }
  }, $lines);
  $csv = array_slice($csv, 1);

  return Yaml::decode($csv);
}

function addKeysToCells($cell, $index) {
  $key = '';
  switch ($index) {
    case 0:
      $key = 'name';
      break;
    case 1:
      $key = 'activity';
      break;
    case 2:
      $key = 'street';
      break;
    case 3:
      $key = 'city';
      break;
    case 4:
      $key = 'region';
      break;
    case 5:
      $key = 'mail';
      break;
    case 6:
      $key = 'network';
      break;
    case 7:
      $key = 'link';
      break;
  }
  return ;
}

function getLocationData($location, $name) {
  $slug = Str::slug($location);
  $query = str_replace(' ', '+', $slug);
  $url = 'https://nominatim.openstreetmap.org/search?format=jsonv2&limit=1&addressdetails=1&q=' . $query;

  $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_USERAGENT => 'idf-tiers-lieux/1.0',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
  ));
  
  $response = json_decode(curl_exec($curl));

  curl_close($curl);
  
  if ($response !== null && count($response) > 0) {
    return $response[0];
  } else {
    throw new Exception("Pas de correspondance trouvée pour l\'adresse: ' . $location . ' (' . $name . ')\n", 500);
  }
}


/**
 * Add location data retrieved through OSM API.
 *
 * @param array $mapData - Array of associative arrays containing 'street' and 'town' properties.
 * @param array|false $oldMapData - Old map data for comparison.
 * @return array - Array of associative arrays with added 'location' property.
 * @throws Exception - Throws exception for invalid data.
 */
function addLocationsData($mapData, $oldMapData = false, $mode = 'site-update') {
  $filePath = './assets/map-data.json';
  
  if (!file_exists($filePath)) {
      file_put_contents($filePath, json_encode([]));
  }
  
  $savedData = json_decode(file_get_contents($filePath));

  $errors = [];
  foreach ($mapData as $i => $item) {
    try {
      if (!isset($item['town']) || empty($item['town'])) {
        throw new Exception('Pas d\'adresse renseignée pour : ' . $item['name'], 400);
      }

      $parenthesisRegex = '/\s*\([^)]*\)/';
      $address = (strlen($item['street']) > 1 ? Html::decode($item['street']) . ', ' : '') . preg_replace($parenthesisRegex, '', $item['town']);
     
      $isAddressChanged = $oldMapData ? ($oldMapData[$i]['street'] !== $item['street'] || $oldMapData[$i]['town'] !== $item['town']) : false;
      $isDataMissing = !$savedData || !$oldMapData;

      if ($mode === 'site-update' && ($isDataMissing || $isAddressChanged)) {
        $mapData[$i]['location'] = getLocationData($address, $item['name']);
      } else {
        $correspondingKey  = array_search($mapData[$i]['name'], array_column($savedData, 'name'));
        $mapData[$i]['location'] = $savedData[$correspondingKey]->location;
      }
       if($mode === 'import') {
        echo "data: " . json_encode(["status" => "processing", "item" => $item, "progress" => $i . ' / ' . count($mapData)]) . "\n\n";
        flush();
       }
    } catch (\Throwable $th) {
      $errors[] = $th->getMessage();

      if ($mode === 'import') {
        echo "data: " . json_encode(["status" => "error", "error" => $th->getMessage()]) . "\n\n";
        flush();
      }
    }
  }
  return [
    'data' => $mapData,
    'errors' => $errors
  ];
}

function convertCoverFiles($yamlData, $covers) {
  return array_map(function ($location, $cover) {
    $location['cover'] = $cover;
    return $location;
  }, $yamlData, $covers);
}

function saveMapDataToJson($newSite, $oldSite = false, $mode = "site-update") {
  $covers = [];
  foreach ($newSite->mapData()->toStructure() as $item) {
    if ($item->cover()->isNotEmpty()) {
      $covers[] = $item->cover()->toFile()->toArray();
    } else {
      $covers[] = false;
    }
  }

  $oldMapData = $oldSite ? convertCoverFiles($oldSite->mapData()->yaml(), $covers) : false;
  $newMapData = convertCoverFiles($newSite->mapData()->yaml(), $covers);

  $locatedMapData = addLocationsData($newMapData, $oldMapData, $mode);

  F::write('./assets/map-data.json', json_encode($locatedMapData['data']));
  
  if (!empty($errors)) {
    throw new Exception("Adresses invalides : " . implode(', ', $locatedMapData['errors']), 1);
  }
}

Kirby::plugin('adrienpayet/kirby-map-block', [
	'blueprints' => [
    'blocks/map' => __DIR__ . '/blueprints/blocks/map.yml',
    'tabs/map' => __DIR__ . '/blueprints/tabs/map.yml',
    'files/csv' => __DIR__ . '/blueprints/files/csv.yml',
  ],
  'fields' => [
    'data-diag' => [
      'computed' => [
        'mapData' => function() {
          return $this->model()->mapData()->toStructure()->toArray();
        }
      ]
    ]
    // 'generate-structure' => [],
  ],
  'hooks' => [
    'site.update:after' => function($newSite, $oldSite) {
      if (json_encode($newSite->mapData()->yaml()) != json_encode($oldSite->mapData()->yaml())) {
        saveMapDataToJson($newSite, $oldSite);
      }
    },
    'file.update:after' => function($newFile) {
      $site = site();
      if ($newFile->parent()->url() == $site->url()) {
        saveMapDataToJson($site, false, 'file-update');
      }
    }
  ],
  'routes' => [
    [
      'pattern' => '/import-map-data.json',
      'action' => function() {

        // Disable buffering
        ob_end_clean();
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');


        $site = site();
        $kirby = kirby();

        if ($site->mapDataFile()->isEmpty()) {
          throw new Exception("aucun fichier sélectionné", 1);
        }

        $csvString = $site->mapDataFile()->toFile()->read();
        $mapData = csvStringToYaml($csvString);

        $kirby->impersonate(kirby()->user()?->id() ?? 'kirby');
        
        $site->update(['mapData' => Yaml::encode($mapData)]);
        
        $locatedMapData = addLocationsData($mapData, false, 'import');
        echo "data: " . json_encode(["status" => "success", "message" => "Map data update succeeded.", "data" => $mapData]) . "\n\n";
        flush();
        F::write('./assets/map-data.json', json_encode($locatedMapData['data']));
      }
    ],
  ]
]);
