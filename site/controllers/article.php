<?php
return function($site, $page) {
  $freePages = $site->index()->filterBy('template', 'free');
  $similarArticles = $freePages->filter(
    function ($item) use($page) {
      $pageTags = $page->tags()->split();
      $itemTags = $item->tags()->split();
      return haveCommonElement($pageTags, $itemTags) && $item->title() !== $page->title();
    }
  );
  return [
    'similarArticles' => $similarArticles
  ];
};