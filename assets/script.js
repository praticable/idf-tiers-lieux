document.addEventListener("DOMContentLoaded", () => {
  function enableBurgerBtn() {
    const burgerBtn = document.querySelector("#menu--mobile__toggle-btn");
    const menuMobile = document.querySelector("#menu--mobile");

    burgerBtn.addEventListener("click", () => {
      document.body.classList.toggle("no-scroll");
      burgerBtn.classList.toggle("open");
      menuMobile.classList.toggle("open");
    });
  }

  function addWhiteLineToContentLinks() {
    const contentLinks = document.querySelectorAll(
      ".block a:not(#map-panel a, .card a, .section-header a)"
    );
    contentLinks.forEach((link) => {
      link.innerHTML =
        '<span class="white-line white-line--link"></span>' + link.innerHTML;
    });
  }

  enableBurgerBtn();
  addWhiteLineToContentLinks();
});
