if (document.querySelector("#map")) {
  (async () => {
    const fetchData = await fetch("/assets/map-data.json");
    if (fetchData.status === 200) {
      const jsonData = await fetchData.json();

      const map = new maplibregl.Map({
        container: "map",
        style:
          "https://api.maptiler.com/maps/f894dc8b-aa76-431b-bad7-355daf924062/style.json?key=1xgYiNMEhOBLFXttdrfa",
        center: [2.333333, 48.7],
        zoom: 8,
      });

      const markers = createMarkers(jsonData);

      const geojson = {
        type: "FeatureCollection",
        features: markers,
      };

      geojson.features.forEach((marker) => {
        if (!marker) return;
        const el = createMarkerElement(marker);

        new maplibregl.Marker({ element: el, anchor: "bottom" })
          .setLngLat(marker.geometry.coordinates)
          .addTo(map);
      });

      document.querySelector("#map-wrapper").addEventListener("click", (e) => {
        const target = e.target;
        if (
          (target.classList.contains("marker") ||
            target.closest("#map-panel")) &&
          !target.closest("#map-panel__close-btn")
        )
          return;

        const mapPanel = document.querySelector("#map-panel");

        mapPanel.classList.remove("open");
        document.querySelector("#map-wrapper").classList.remove("open");

        const activeMarker = document.querySelector(".marker.active");

        if (activeMarker) {
          activeMarker.classList.remove("active");
        }
      });

      function createMarkers(jsonData) {
        const markers = [];
        jsonData.forEach((item, key) => {
          try {
            const marker = createMarker(item, markers);
            markers.push(marker);
          } catch (error) {
            console.error(`${item.name} (line ${key + 1}) :`, error.message);
          }
        });
        return markers;
      }

      function createMarker(item, markers) {
        if (!item.hasOwnProperty("location") || !item.location) {
          throw new Error("Location missing");
        }

        const marker = {
          type: "Feature",
          properties: {
            activity: item.activity.split(", "),
            message: item.name,
            iconSize: [20, 30],
            title: item.name,
            description: item.description,
            address: `${item.street.split(" ")[0]} ${
              item.location.address.road
            } ${item.location.address.postcode} ${
              item.location.address.town || item.location.address.city
            }`,
            cover: item.cover,
            link: item.link,
            linkLabel: item.linklabel || false,
            network: item.network,
            mail: item.mail,
          },
          geometry: {
            type: "Point",
            coordinates: [
              parseFloat(item.location.lon),
              parseFloat(item.location.lat),
            ],
          },
        };

        return marker;
      }

      function adjustMarkerPosition(marker, markers, dimension = "latitude") {
        let adjustedCoordinate =
          dimension === "latitude"
            ? marker.geometry.coordinates[1]
            : marker.geometry.coordinates[0];

        const minOffset = 0.002;
        const maxOffset = 0.004;
        const offset = minOffset + Math.random() * (maxOffset - minOffset);

        while (
          markers.some((m) => {
            if (m && m !== marker) {
              const coordinateDiff = Math.abs(
                dimension === "latitude"
                  ? m.geometry.coordinates[1] - adjustedCoordinate
                  : m.geometry.coordinates[0] - adjustedCoordinate
              );
              return coordinateDiff < offset;
            }
            return false;
          })
        ) {
          adjustedCoordinate += offset;
        }

        return adjustedCoordinate;
      }

      function createMarkerElement(marker) {
        if (!marker) return;
        const el = document.createElement("div");
        el.className = "marker";
        el.style.width = `${marker.properties.iconSize[0]}px`;
        el.style.height = `${marker.properties.iconSize[1]}px`;

        el.addEventListener("click", () => {
          const panel = document.querySelector("#map-panel");

          if (document.querySelector(".marker.active")) {
            document.querySelector(".marker.active").classList.remove("active");
          }
          el.classList.add("active");

          fillPanel(marker, panel);

          console.log(window.innerWidth);

          const centerFactor = window.innerWidth > 800 ? 0.04 : 0;
          console.log(centerFactor);

          map.flyTo({
            center: [
              marker.geometry.coordinates[0] - centerFactor,
              marker.geometry.coordinates[1],
            ],
            zoom: 12.5,
          });

          panel.classList.add("open");
          document.querySelector("#map-wrapper").classList.add("open");
        });

        return el;
      }

      function fillPanel(marker, panel) {
        console.log(marker);

        const panelTitle = document.querySelector("#map-panel__title");
        const panelDescription = document.querySelector(
          "#map-panel__description"
        );
        const panelAddress = document.querySelector("#map-panel__address");
        const panelWebsite = document.querySelector("#map-panel__website a");
        const panelTags = document.querySelector("#map-panel__tags");
        const panelNetwork = document.querySelector(
          "#map-panel__network .value"
        );
        const panelMail = document.querySelector("#map-panel__mail a .value");
        const panelCoverWrapper = document.querySelector("#map-panel__cover");
        const panelCoverImg = document.querySelector("#map-panel__cover img");

        panelTitle.textContent = marker.properties.title;
        panelDescription.textContent = marker.properties.description;

        panelAddress.textContent = marker.properties.address;

        marker.properties.network.length > 0
          ? panelNetwork.closest("li").classList.remove("hidden")
          : panelNetwork.closest("li").classList.add("hidden");
        panelNetwork.textContent = marker.properties.network;

        marker.properties.mail.length > 0
          ? panelMail.closest("li").classList.remove("hidden")
          : panelMail.closest("li").classList.add("hidden");
        panelMail.parentNode.href = "mailto:" + marker.properties.mail;
        panelMail.textContent = marker.properties.mail;

        marker.properties.link.length > 0
          ? panelWebsite.closest("li").classList.remove("hidden")
          : panelWebsite.closest("li").classList.add("hidden");
        panelWebsite.href = marker.properties.link;
        panelWebsite.textContent = marker.properties.linkLabel
          ? marker.properties.linkLabel
          : "site web";

        panelMail.textContent = marker.properties.mail;

        const delay = panelCoverWrapper.classList.contains("collapse")
          ? 0
          : 300;
        panelCoverWrapper.classList = marker.properties.cover ? "" : "collapse";
        setTimeout(() => {
          panelCoverImg.src = marker.properties.cover
            ? marker.properties.cover.url
            : "";
        }, delay);

        const tags = marker.properties.activity;
        const ul = document.querySelector("#map-panel__tags");
        ul.innerHTML = "";
        tags.forEach((tag) => {
          if (tag.length === 0) return;
          const li = document.createElement("li");
          li.classList.add("tag");
          li.textContent = tag;
          ul.appendChild(li);
        });
      }

      // =========================================== LOCKER

      function lockMap() {
        let timeoutId;

        function enableLocker() {
          lockerNode.classList.remove("disabled");
        }

        function startTimer() {
          clearTimeout(timeoutId);
          timeoutId = setTimeout(enableLocker, 1500);
        }

        const mapNode = document.querySelector("#map");
        mapNode.addEventListener("mouseenter", startTimer);
        mapNode.addEventListener("mouseleave", () => clearTimeout(timeoutId));

        map.on("move", startTimer);
        map.on("zoom", startTimer);
      }

      lockMap();

      const lockerNode = document.querySelector("#map-locker");

      lockerNode.addEventListener("click", () =>
        lockerNode.classList.add("disabled")
      );
    }
  })();
}
