<?php
  /** @var \Kirby\Cms\Block $block */
  $file = $block->file()->toFile();
  $href = $file->url();
  $name = $file->name();
  $type = Str::upper($file->extension());
  $size = $file->niceSize();
?>

<div class="block block--dl-file">
  <a class="block--dl-file__link text-1" href="<?= $block->file()->toFile()->url() ?>" download>
    <span class="white-line white-line--link"></span>
    <?= $name ?>
  </a>
  <div class="block--dl-file__info text-4">
    <span class="block--dl-file__type"><?= $type ?></span>
    <span class="block--dl-file__size"><?= $size ?></span>
  </div>
</div>