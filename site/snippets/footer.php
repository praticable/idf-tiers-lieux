<div class="divider">
  <hr />
  <span class="white-line white-line--divider"></span>
</div>
<footer id="main-footer">
  <figure id="footer__logo">
    <p id="footer-logo__first-part">Île-de-France</p>
    <p id="footer-logo__second-part">
      Tiers-<br>
      lieux
    </p>
    <img id="main-footer__logo" src="<?= url('assets') ?>/svg/logo.svg" alt="">
  </figure>
  <div id="main-footer__plan">
    <h4>Plan du site</h4>
    <ul>
    <?php foreach ($site->children()->listed() as $child): ?>
      <?php
        $isExternal = $child->template() == 'default';
        $url = $isExternal ? $child->link() : $child->url();
      ?>
      <li>
        <a href="<?= $url ?>" <?php e($isExternal, 'target="_blank"'); ?>>
          <span class="white-line white-line--link"></span>
          <?= $child->title() ?>
        </a>
      </li>
    <?php endforeach ?>
    </ul>
  </div>
  <div id="main-footer__contact">
    <h4>Contact</h4>
    <ul>
      <?php if ($site->address()->isNotEmpty()): ?>
        <li class="address" id="main-footer__address">
          <p>
            <?= $site->address() ?>
          </p>
        </li>
      <?php endif ?>
      
      <?php if ($site->mail()->isNotEmpty()): ?>
        <li id="main-footer__mail" class="mail">
          <a href="mailto:<?= $site->mail() ?>">
            <span class="white-line white-line--link"></span>
            <?= $site->mail() ?>
          </a>
        </li>
      <?php endif ?>

      <?php if ($site->tel()->isNotEmpty()): ?>
        <li id="main-footer__tel">
          <a href="tel:<?= $site->tel() ?>">
            <span class="white-line white-line--link"></span>
            <?= $site->tel() ?>
          </a>
        </li>
      <?php endif ?>
    </ul>
    <p class="main-footer__info text-4">
    Sauf mention contraire, le design de ce site et les contenus de ce site sont mis sous Licence Creative Commons BY-NC-SA 3.0 . Design par Anthony Ferretti et Morgane Chevalier. Développement par Adrien Payet.
    </p>
  </div>
</footer>
  
</body>
</html>