<?php

Kirby::plugin('adrienpayet/side-by-side-images', [
	'blueprints' => [
    'blocks/side-by-side-images' => __DIR__ . '/blueprints/blocks/side-by-side-images.yml'
  ],
  'snippets' => [
    'blocks/side-by-side-images' => __DIR__ . '/snippets/blocks/side-by-side-images.php'
  ]
]);
