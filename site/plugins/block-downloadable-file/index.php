<?php

Kirby::plugin('adrienpayet/block-downloadable-file', [
	'blueprints' => [
    'blocks/downloadable-file' => __DIR__ . '/blueprints/blocks/downloadable-file.yml',
    'files/downloadable-file' => __DIR__ . '/blueprints/files/file.yml'
  ],
  'snippets' => [
    'blocks/downloadable-file' => __DIR__ . '/snippets/blocks/downloadable-file.php'
  ]
]);
