<div class="block--side-by-side-images">
  <?php if ($leftImage = $block->leftImage()->toFile()): ?>
  <figure class="block--image">
    <div class="image-wrapper">
      <?= $leftImage ?>
    </div>

    <?php if ($leftImage->caption()->isNotEmpty()): ?>
    <figcaption>
      <?= $leftImage->caption() ?>
    </figcaption>
    <?php endif ?>
  </figure>
  <?php endif ?>
  <?php if ($rightImage = $block->rightImage()->toFile()): ?>
  <figure class="block--image">
    <div class="image-wrapper">
      <?= $rightImage ?>
    </div>

    <?php if ($rightImage->caption()->isNotEmpty()): ?>
    <figcaption>
      <?= $rightImage->caption() ?>
    </figcaption>
    <?php endif ?>
  </figure>
  <?php endif ?>
</div>