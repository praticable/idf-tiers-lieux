<?php
  $target = $block->representative()->toPage();
  $template = $target->template();
?>
<?php if ($template == 'free'): ?>
  <section class="block block--representative representative--free">
    <header class="section-header">
      <h2><?= $target->title() ?></h2>
      <?php snippet('button', [
        'text' => 'En savoir plus', 
        'modifiers' => ['text', 'arrow'],
        'href' => $target->url()
      ]) ?>
    </header>
    <div class="grid">
      <div class="intro column" style="--span:3">
          <?= $target->intro() ?>
      </div>
    </div>
  </section>
<?php endif ?>

<?php if ($template == 'repository'): ?>
  <section class="block block--representative representative--repository" style="--span:3">
    <header class="section-header">
      <div class="grid">
        <div class="column" style="--span: 3;">
          <h2><?= $target->title() ?></h2>
          <?= $target->intro() ?>
        </div>
        <?php snippet('button', [
          'text' => 'Tout voir', 
          'modifiers' => ['text', 'arrow'],
          'href' => $target->url()
        ]) ?>
      </div>
    </header>
    <div class="cards-slider">
      <?php foreach($target->children()->limit(4) as $child): ?>
          <?php snippet('card', [
            'target' => $child
          ]) ?>
      <?php endforeach ?>
    </div>
  </section>
<?php endif ?>