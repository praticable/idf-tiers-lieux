<div id="map-wrapper">
  <div id="map-locker">
    <p>cliquez<br>pour interagir</p>
  </div>
<div id="map-panel">

  <button id="map-panel__close-btn">
    <span id="map-panel__close-btn__white-line"></span>
    <img src="/assets/svg/close.svg" alt="">
  </button>
  <div id="map-panel__content-wrapper">
    <figure id="map-panel__cover" class="collapse">
      <div class="color-filter"></div>
      <img src="" alt="">
    </figure>
    <div id="map-panel__content">
      <h3 id="map-panel__title"></h3>
      <p id="map-panel__description" class="intro"></p>
      <ul id="map-panel__metadata">
        <li class="address">
          <p id="map-panel__address" class="text-3"></p>
        </li>
        <li id="map-panel__website">
          <a class="text-3" target="_blank">
            <span class="white-line white-line--link"></span>
            <span class="value"></span>
          </a>
        </li>
        <li id="map-panel__network" class="mail">
          <p class="text-3">Réseau : <span class="value"></span></p>
        </li>
        <li id="map-panel__mail" class="mail">
          <a class="text-3" target="_blank" rel="noopener noreferrer">
            <span class="white-line white-line--link"></span>
            <span class="value"></span>
          </a>
        </li>
      </ul>
      <ul id="map-panel__tags" class="tags">
          
      </ul>
    </div>
  </div>
</div>
<div class="block block--map" id="map">
</div>
</div>