<?php
  $idAttribute = isset($id) ? 'id="' . $id . '"' : false;
  $colorFilter = isset($colorFilter) ? $colorFilter : false;
  $caption = isset($caption) && strlen($caption) > 0 ? $caption : false;
?>
<figure  <?php e($idAttribute, $idAttribute) ?> >
  <div class="image-wrapper">
    <?php if ($colorFilter): ?>
      <div class="color-filter"></div>
    <?php endif ?>
    <img src="<?= $src ?>" alt="">
  </div>
  <?php if ($caption): ?>
      <figcaption><?= $caption ?></figcaption>
  <?php endif ?>
</figure>