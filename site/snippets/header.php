<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php snippet('meta') ?>
  <title>
    <?php if ($page->isHomePage() == false): ?>
        <?= $page->title() ?>&nbsp;-&nbsp;
    <?php endif ?>
    IDF Tiers-Lieux
  </title>

  <link rel="stylesheet" href="<?= url('assets') ?>/style.css">

  <script src="/assets/map.js" defer></script>
  <script src="/assets/script.js" defer></script>
  <link href='https://unpkg.com/maplibre-gl@latest/dist/maplibre-gl.css' rel='stylesheet' />
  <script src='https://unpkg.com/maplibre-gl@latest/dist/maplibre-gl.js'></script>

  <!--========== FONTS ==========-->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&family=Recursive:wght@400;600;700&display=swap" rel="stylesheet">
  <?= e($page->template() == 'repository', '<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>') ?>
  <meta name="robots" content="noindex">
  <?php snippet('front-comments-style') ?>
  <?php snippet('favicon') ?>
</head>
<body>
  <?php snippet('front-comments') ?>
  <nav id="main-header">
    <a href="/" id="logo-wrapper">
      <h1 id="logo-title" class="text-2 desktop">Île-de-France Tiers-Lieux</h1>
      <img id="main-header__logo" src="<?= url('assets') ?>/svg/logo-header.svg" alt="">
      <span id="main-header__logo-line" class="desktop"></span>
    </a>
    <ul id="menu" class="desktop">
    <?php foreach ($site->children()->listed() as $child) : ?>
      <?php
        $isExternal = $child->template() == 'default';
        $url = $isExternal ? $child->link() : $child->url();
      ?>
      <li>
        <a href="<?= $url ?>" class="menu__item text-2" <?php e($isExternal, 'target="_blank"'); ?>><?= $child->title() ?></a>
      </li>
    <?php endforeach ?>
    </ul>
      <?php snippet('button', [
        'text' => 'Menu',
        'id' => 'menu--mobile__toggle-btn',
        'variant' => 'mobile',
        'burger' => 'true'
      ]) ?>
      <?php endsnippet() ?>
      <ul id="menu--mobile" class="mobile">
        <?php foreach ($site->children()->listed() as $child) : ?>
          <?php
            $isExternal = $child->template() == 'default';
            $url = $isExternal ? $child->link() : $child->url();
          ?>
          <li>
            <a href="<?= $url ?>" class="menu__item text-2" <?php e($isExternal, 'target="_blank"'); ?>><?= $child->title() ?></a>
          </li>
        <?php endforeach ?>
      </ul>
  </nav>