<?php snippet('header') ?>
<main x-data="{
  search: '',
  filters: [],
  get isNoSearch() {
    const isNoSearch = this.search.length === 0 && this.filters.length === 0
    return isNoSearch
  },
  toggleTag(tag) {
    if (this.filters.some(filter => filter === tag)) {
      this.filters = this.filters.filter(filter => filter !== tag)
    } else {
      this.filters.push(tag)
    }
  }
}">
  <header id="repository-header" class="main__intro">
    <h1 id="page-title"><?= $page->title() ?></h1>
      <?= $page->intro() ?>
  </header>
  <section id="manage">
    <?php if (count($tags) > 0): ?>
      <h5>Filtrer et rechercher</h5>
    <?php endif ?>
    <div id="manage__buttons">
      <?php if (count($tags) > 0): ?>
        <ul class="tags">
        <?php foreach($tags as $tag): ?>
          <li>
            <button class="tag tag--large" :class="selected ? 'selected' : ''" x-data="{
              selected: false,
            }" @click="toggleTag('<?= $tag ?>'), selected = ! selected"><?= $tag ?> <img class="btn-icon" :src="selected ? '/assets/svg/close.svg' : '/assets/svg/plus.svg'" alt=""></button>
          </li>
        <?php endforeach ?>
        </ul>
      <?php endif ?>
      <div x-data="{ 
        open: false,
        openSearch() {
          this.open = true
          $refs.input.focus()
        },
        closeSearch() {
          this.search = ''
          this.open = false
        }
      }" id="search" :class="open ? 'open' : 'close'" @click.outside="open = false" @click="openSearch()">
          <button id="search__open"><img src="/assets/svg/search.svg" alt=""></button>
          <input id="search__field" type="text" placeholder="Rechercher…" x-model="search" @keyup.escape="closeSearch()" x-ref="input">
          <button id="search__clear" :class="search.length === 0 ? 'hidden' : ''" @click="search = ''"><img src="/assets/svg/close.svg" alt=""></button>
      </div>
    </div>
  </section>
  <?php snippet('divider') ?>
  <ul id="cards__wrapper">
    <?php foreach($page->children() as $child): ?>
      <?php snippet('card', [
          'name' => 'card',
          'target' => $child
        ]) ?>
    <?php endforeach ?>
  </ul>
  <?php snippet('newsletter') ?>
</main>
<?php snippet('footer') ?>