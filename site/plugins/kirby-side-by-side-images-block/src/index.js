import SideBySideImages from "./components/SideBySideImages.vue";

window.panel.plugin("getkirby/pluginkit", {
  blocks: {
    "side-by-side-images": SideBySideImages,
  },
});
