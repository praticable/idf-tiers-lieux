<?php snippet('header') ?>
<main>
  <article>
    <header id="article-header" class="main__intro">
      <h1 id="page-title"><?= $page->title() ?></h1>
      <?php if ($page->intro()->isNotEmpty()): ?>
          <?= $page->intro() ?>
      <?php endif ?>
      
      
      <div id="page-infos">
        <span class="page-info"><?= $page->published()->toDate('d/m/Y') ?></span>
        <?php if ($page->author()->isNotEmpty()): ?>
          <span class="page-info">par <?= $page->author()->toUser()->username() ?></span>
        <?php endif ?>
        <?php if ($page->linkUrl()->isNotEmpty()): ?>
          <br class="mobile">
          <a class="page-info" href="<?= $page->linkUrl()->toUrl() ?>" target="_blank">
            <span class="white-line white-line--link"></span>
            <?= $page->linkText() ?>
          </a>
        <?php endif ?>
      </div>
      <?php if($cover = $page->cover()->toFile()): ?>
        <?php snippet('image', [
          'id' => 'page-cover',
          'colorFilter' => true,
          'src' => $cover->url(),
          'caption' => $cover->caption()
        ]) ?>
      <?php endif ?>
      <?php snippet('divider') ?>
    </header>
    <div id="article-body" class="page__body">
    <?php foreach ($page->builder()->toLayouts() as $layout): ?>
    <section class="grid" id="<?= $layout->id() ?>">
      <?php foreach ($layout->columns() as $column): ?>
      <div class="column" style="--span:<?= $column->span(4) ?>">
        <div class="blocks">
          <?php foreach ($column->blocks() as $block): ?>
          <div class="block block-type-<?= $block->type() ?>">
            <?php snippet('blocks/' . $block->type(), ['block' => $block, 'layout' => $layout]) ?>
          </div>
          <?php endforeach ?>
        </div>
      </div>
      <?php endforeach ?>
    </section>
    <?php endforeach ?>
      <ul class="tags">
        <?php foreach($page->tags()->split() as $tag): ?>
          <li class="tag"><?= $tag ?></li>
        <?php endforeach ?>
      </ul>
    </div>
  </article>
  <?php if (count($similarArticles) > 0): ?>
    <?php snippet('divider') ?>
    <section id="similar-articles">
      <header class="section-header">
        <h2>Articles similaires</h2>
        <?php snippet('button', [
          'text' => 'Tout voir', 
          'modifiers' => ['text', 'arrow']
        ]) ?>
      </header>
      <ul id="cards__wrapper">
        <?php foreach($similarArticles as $similarArticle): ?>
          <?php snippet('card', [
              'target' => $similarArticle
            ]) ?>
        <?php endforeach ?>
      </ul>
    </section>
  <?php endif ?>
  <?php snippet('newsletter') ?>
</main>
<?php snippet('footer') ?>