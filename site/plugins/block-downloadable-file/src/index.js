import DownloadableFileBlock from "./components/DownloadableFileBlock.vue";

window.panel.plugin("adrienpayet/block-downloadable-file", {
  blocks: {
    "downloadable-file": DownloadableFileBlock,
  },
});
