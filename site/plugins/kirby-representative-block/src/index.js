import Representative from "./components/Representative.vue";

window.panel.plugin("adrienpayet/kirby-representative-block", {
  blocks: {
    representative: Representative,
  },
});
