<?php
return function($page) {
  $tags = [];
  foreach ($page->children() as $child) {
    foreach ($child->tags()->split() as $tag) {
      if (!in_array($tag, $tags)) {
        $tags[] = $tag;
      }
    }
  }
  return [
    'tags' => $tags
  ];
};