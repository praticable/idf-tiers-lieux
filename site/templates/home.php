<?php snippet('header') ?>
<main id="home-main">
  <article>
    <header class="grid-wrapper" id="home-header">
      <div id="home-header__left-col">
        <h1><?= $site->title() ?></h1>
        <p id="page-intro">
          <?= Str::unhtml($page->intro()) ?>
        </p>
        
        <?php if ($page->buttonText()->isNotEmpty() == true && $page->buttonLink()->isNotEmpty() == true) {
          snippet('button', [
            'text' => $page->buttonText(),
            'modifiers' => ['arrow'],
            'id' => 'home-header__button',
            'href' => $page->buttonLink()
          ]);
        } ?>
        <a href="#section-1" class="text-2 desktop" id="scroll-down-btn">
          Défiler vers le bas
        </a>
      </div>
      
      <div id="home-header__right-col">
        <?php 
        $cover = $page->cover()->toFile();
        snippet('image', [
          'src' => $cover->url(),
          'caption' => $cover->caption(),
          'colorFilter' => true,
          'id' => 'home-header__cover'
        ]) ?>
      </div>
    </header>
    <?php $i = 0 ?>
    <?php foreach ($page->builder()->toLayouts() as $layout): ?>
      <?php $i++ ?>
      <section class="grid" id="section-<?= $i ?>">
        <?php foreach ($layout->columns() as $column): ?>
        <div class="column" style="--span:<?= $column->span(4) ?>">
          <div class="blocks">
            <?php foreach ($column->blocks() as $block): ?>
            <div class="block block-type-<?= $block->type() ?>">
              <?php snippet('blocks/' . $block->type(), ['block' => $block, 'layout' => $layout]) ?>
            </div>
            <?php endforeach ?>
          </div>
        </div>
        <?php endforeach ?>
      </section>
    <?php endforeach ?>
  </article>
</main>
<?php snippet('footer') ?>